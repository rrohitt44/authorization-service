INSERT INTO User (id, username, password, salary, age) VALUES (1, 'Rohit', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 3456, 33);
INSERT INTO User (id, username, password, salary, age) VALUES (2, 'Ankit', '$2a$04$PCIX2hYrve38M7eOcqAbCO9UqjYg7gfFNpKsinAxh99nms9e.8HwK', 7823, 23);
INSERT INTO User (id, username, password, salary, age) VALUES (3, 'Abhijeet', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 4234, 45);
INSERT INTO User (id, username, password, salary, age) VALUES (4, 'Guddi', '$2a$04$I9Q2sDc4QGGg5WNTLmsz0.fvGv3OjoZyj81PrSFyGOqMphqfS2qKu', 4234, 45);

INSERT INTO roles(id,description,name) values (1,'Admin', 'ADMIN');
INSERT INTO roles(id, description,name) values (2, 'User', 'USER');

insert into USER_ROLES(USER_ID,ROLE_ID) values (1,1);
insert into USER_ROLES(USER_ID,ROLE_ID) values (2,1);
insert into USER_ROLES(USER_ID,ROLE_ID) values (3,1);
insert into USER_ROLES(USER_ID,ROLE_ID) values (4,2);
insert into USER_ROLES(USER_ID,ROLE_ID) values (1,2);