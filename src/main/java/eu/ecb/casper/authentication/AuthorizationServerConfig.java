package eu.ecb.casper.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

/**
 * @author RohitMuneshwar
 *
 * This class extends AuthorizationServerConfigurerAdapter and is responsible for 
 * generating tokens specific to a client.
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter
{
	private static final String CLIENT_ID = "perpixl-client";
	// devglan-secret: For Spring Boot 2 you need to Bcrypt CLIENT_SECRET. //"perpixl-secret";
	//"$2a$04$e/c1/RfsWuThaWFCrcCuJeoyvwCV0URN/6Pn9ZFlrtIWaU/vj/BfG";
	private static final String CLIENT_SECRET = "$2a$04$e/c1/RfsWuThaWFCrcCuJeoyvwCV0URN/6Pn9ZFlrtIWaU/vj/BfG";
	private static final String GRANT_TYPE_PASSWORD = "password";
	private static final String AUTHORIZATION_CODE = "authorization_code";
	private static final String REFRESH_TOKEN = "refresh_token";
	private static final String IMPLICIT = "implicit";
	private static final String SCOPE_READ = "read";
	private static final String SCOPE_WRITE = "write";
	private static final String TRUST = "trust";
	private static final int ACCESS_TOKEN_VALIDITY_SECONDS = 1*60*60;
    private static final int FREFRESH_TOKEN_VALIDITY_SECONDS = 6*60*60;
    
    @Autowired
    private TokenStore tokenStore;
    
    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Bean
    public JwtAccessTokenConverter accessTokenConvertor()
    {
    	JwtAccessTokenConverter convertor = new JwtAccessTokenConverter();
    	convertor.setSigningKey("as466gf");
    	return convertor;
    }
    
    @Bean
	public TokenStore tokenStore()
	{
		// InMemoryTokenStore is the in memory default token store as a token provider.
		// Spring Boot also allows us to configure custom token stores such as JwtTokenStore
		//return new InMemoryTokenStore();
    	
    	// JwtTokenStore:
    	// A JWT token consists of 3 parts seperated with a dot(.) i.e. Header.payload.signature
    	return new JwtTokenStore(accessTokenConvertor());
	}
	
    
    /* (non-Javadoc)
     * @see org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer)
     * 
     * ClientDetailsServiceConfigurer:
     * A configurer that defines the client details service.
     * Client details can be initialized, or you can just refer to an existing store.
     * ClientDetailsServiceConfigurer can be used to define an in-memory or 
     * JDBC implementation of the client details service.
     * In this example, we are using an in-memory implementation.
     */
    @Override
    	public void configure(ClientDetailsServiceConfigurer clients) throws Exception
    	{
	    	clients.inMemory()
	    	.withClient(CLIENT_ID)
	    	.secret(CLIENT_SECRET)
	    	.authorizedGrantTypes(GRANT_TYPE_PASSWORD, AUTHORIZATION_CODE, REFRESH_TOKEN, IMPLICIT)
	    	.scopes(SCOPE_READ, SCOPE_WRITE, TRUST)
	    	.accessTokenValiditySeconds(ACCESS_TOKEN_VALIDITY_SECONDS)
	    	.refreshTokenValiditySeconds(FREFRESH_TOKEN_VALIDITY_SECONDS);
    	}
    
    /* (non-Javadoc)
     * @see org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer)
     * 
     * AuthorizationServerEndpointsConfigurer:
     * defines the authorization and token endpoints and the token services.
     */
    @Override
    	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception
    	{
    		endpoints.tokenStore(tokenStore)
    		.authenticationManager(authenticationManager)
    		.accessTokenConverter(accessTokenConvertor());
    	}
    
    /* (non-Javadoc)
     * @see org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer)
     * 
     * AuthorizationServerSecurityConfigurer:
     * defines the security constraints on the token endpoint.
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception
    {
    	super.configure(security);
    }
}
