package eu.ecb.casper.authentication;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

/**
 * @author RohitMuneshwar
 *
 * Resource in our context is the REST API which we have exposed for the crud operation.
 * To access these resources, client must be authenticated.
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter
{
	private static final String RESOURCE_ID = "resource_id";
	
	@Override
		public void configure(ResourceServerSecurityConfigurer resources) throws Exception
		{
			resources.resourceId(RESOURCE_ID);
		}
	
	@Override
		public void configure(HttpSecurity http) throws Exception
		{
		http.anonymous().disable()
		.authorizeRequests()
		// Here, we have configured that /users is a protected resource and it requires an ADMIN role for the access.
		.antMatchers("/users/**").access("hasRole('ADMIN')")
		.antMatchers("/admin/**").access("hasRole('ADMIN')")
		.and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
		}
}
