package eu.ecb.casper.authentication.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import eu.ecb.casper.authentication.dao.UserRepository;
import eu.ecb.casper.authentication.entity.Role;
import eu.ecb.casper.authentication.entity.User;

@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService
{

	@Autowired
	private UserRepository userDao;

	public UserDetails loadUserByUsername(String userId) throws UsernameNotFoundException {
		User user = userDao.findByUsername(userId);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		Set grantedAuthorities = getAuthorities(user);
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
	}

	private Set getAuthorities(User user)
	{
		Set<Role> roleByUserId = user.getRoles();
		final Set authorities = (Set) roleByUserId.stream().map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName().toString().toUpperCase())).collect(Collectors.toSet());
        return authorities;
	}

	private List getAuthority() {
		return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List findAll() {
		List list = new ArrayList<>();
		userDao.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	public User save(User user)
	{
		return userDao.save(user);
	}

	public void delete(Long id)
	{
		userDao.deleteById(id);
	}

	public User get(Long id)
	{
		return userDao.findById(id).orElse(null);
	}

}
