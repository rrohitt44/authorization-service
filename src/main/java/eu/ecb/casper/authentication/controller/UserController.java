package eu.ecb.casper.authentication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import eu.ecb.casper.authentication.entity.User;
import eu.ecb.casper.authentication.service.UserServiceImpl;

@RestController
@RequestMapping("/users")
public class UserController {

	public static final String SUCCESS = "success";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_USER = "ROLE_USER";
    
    @Autowired
    private UserServiceImpl userService;

    @Secured({ROLE_ADMIN})
    @RequestMapping(value="/user", method = RequestMethod.GET)
    public List listUser(){
        return userService.findAll();
    }
    
    @Secured({ROLE_ADMIN, ROLE_USER})
    @RequestMapping(value="/user/{id}", method = RequestMethod.GET)
    public User listUser(@PathVariable(value = "id") Long id){
        return userService.get(id);
    }

    @Secured({ROLE_ADMIN})
    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public User create(@RequestBody User user){
        return userService.save(user);
    }

    @Secured({ROLE_ADMIN, ROLE_USER})
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable(value = "id") Long id){
        userService.delete(id);
        return "success";
    }

}
