package eu.ecb.casper.authentication.entity;

public enum RoleType
{
	ADMIN, USER
}
