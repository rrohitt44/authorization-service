package eu.ecb.casper.authentication.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import eu.ecb.casper.authentication.entity.User;

public interface UserRepository extends JpaRepository<User, Long>
{
	public User findByUsername(String username);
}
